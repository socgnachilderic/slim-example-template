function createBarLine() {
    const bars = document.querySelectorAll(".bars");

    if (bars.length > 0) {
        for (const bar of bars) {
            const spanLang = document.createElement("span");
            const spanPercent = document.createElement("span");

            const info = document.createElement("div");
            const line = document.createElement("div")
        
            spanLang.textContent = bar.dataset.lang;
            spanPercent.textContent = bar.dataset.percent;

            info.className = "info mb-1";
            line.className = "line";
            line.style.setProperty("--bar-line-width", bar.dataset.percent)

            info.appendChild(spanLang);
            info.appendChild(spanPercent);
            
            bar.appendChild(info);
            bar.appendChild(line);
        }
    }
}

class Carousel {

    /**
     * @param {Object} options
     * @param {number} [options.slidesToScroll=1] Nombre d'élément à faire défiler
     * @param {number} [options.slidesVisible=1] Nombre d'élément visible dans un slide
     * @param {boolean} [options.loop=false] Le carousel doit il boucler
     * @param {boolean} [options.infinite=false]
     * @param {boolean} [options.pagination=false]
     * @param {boolean} [options.navigation=true]
     */
    constructor(element, options = {}) {
        this.options = Object.assign({}, {
            slidesToScroll: 1,
            slidesVisible: 1,
            loop: false,
            infinite: false,
            pagination: false,
            navigation: true,
        }, options)
        this.element = element;
        
        // const children = [].slice.call(element.children)
        this.isMobile = false
        this.currentItem = 0
        this.offset = 0
        this.moveCallbacks = []
    
        if (!element.classList.contains("carousel")) 
            element.classList = "carousel " + element.classList

        this.container = this.element.querySelector('.carousel-container')
        this.items = [].slice.call(this.element.querySelectorAll('.carousel-container > .carousel-item'))
        this.element.setAttribute("tabindex", '0')

        if (this.options.infinite) {
            this.offset = this.options.slidesVisible * 2 - 1
            this.items = [
                ...this.items.slice(this.items.length - this.offset).map(item => item.cloneNode(true)),
                ...this.items,
                ...this.items.slice(0, this.offset).map(item => item.cloneNode(true)),
            ]
            // this.currentItem = offset
            this.gotoItem(this.offset, false)
        }

        this.items.forEach(item => this.container.appendChild(item))
        this.setStyle()
        if (this.options.navigation) this.createNavigation()
        if (this.options.pagination) this.createPagination()

        // // Events
        this.moveCallbacks.forEach(cb => cb(this.currentItem))
        this.onWindowResize()
        window.addEventListener('resize', this.onWindowResize.bind(this))
        this.element.addEventListener("keyup", e => {
            if (e.key === "ArrowRight") this.nextActionButton()
            else if (e.key === "ArrowLeft") this.prevActionButton()
        })

        if (this.options.infinite) this.container.addEventListener("transitionend", this.onResetInfinite.bind(this))
    }

    setStyle() {
        const ratio = this.items.length / this.slidesVisible
        this.container.style.width = (ratio * 100) + "%"
        this.items.forEach(item => item.style.width = ((100 / this.slidesVisible) / ratio) + "%");
    }

    createNavigation() {
        const nextButton = this.element.querySelector('.carousel-control .carousel-next')
        const prevButton = this.element.querySelector('.carousel-control .carousel-prev')

        nextButton.addEventListener('click', this.nextActionButton.bind(this))
        prevButton.addEventListener('click', this.prevActionButton.bind(this))

        if (!this.options.loop) {
            this.onMove(index => {
                prevButton.classList.toggle("carousel-prev--hidden", index === 0)
                nextButton.classList.toggle("carousel-next--hidden", this.items[this.currentItem + this.slidesVisible] === undefined)
            })
        }
    }

    createPagination() {
        const pagination = this.createDivWithClass('carousel-pagination')
        this.element.querySelector('.carousel-control').appendChild(pagination)
        const paginationButtons = []
        for (let i = 0; i < (this.items.length - 2 * this.offset); i = i + this.options.slidesToScroll) {
            let button = this.createDivWithClass('carousel-dot')
            button.addEventListener('click', () => this.gotoItem(i + this.offset))
            pagination.appendChild(button)
            paginationButtons.push(button)
        }
        this.onMove(index => {
            const activeButton = paginationButtons[Math.floor((index - this.offset) / this.options.slidesToScroll)]
            if (activeButton) {
                paginationButtons.forEach(button => button.classList.remove('active'))
                activeButton.classList.add('active')
            }
        })
    }

    nextActionButton() {
        this.gotoItem(this.currentItem + this.slidesToScroll)
    }

    prevActionButton() {
        this.gotoItem(this.currentItem - this.slidesToScroll)
    }

    /**
     * Déplace le carousel vers l'élément ciblé
     * @param {number} index 
     * @param {boolean} [animation = true]
     */
    gotoItem(index, animation = true) {
        if (index < 0) {
            if (this.options.loop) index = this.items.length - this.slidesVisible
            else return
        } else if (index >= this.items.length || (this.items[this.currentItem + this.slidesVisible] === undefined && index > this.currentItem)) {
            if (this.options.loop) index = 0
            else return
        }
        const translateX = index * -100 / this.items.length
        if (!animation) this.container.style.transition = 'none'
        this.container.style.transform = 'translate3d('+translateX+'%, 0, 0)'
        this.container.offsetHeight // force repaint
        if (!animation) this.container.style.transition = ''
        this.currentItem = index
        this.moveCallbacks.forEach(cb => cb(index))
    }

    /**
     * Déplace le container pour donner l'impression d'un slide infini
     */
    onResetInfinite () {
        const reposition = this.items.length - 2 * this.offset
        if (this.currentItem <= this.options.slidesToScroll) {
            this.gotoItem(this.currentItem + reposition, false)
        } else if (this.currentItem >= this.items.length - this.offset) {
            this.gotoItem(this.currentItem - this.reposition, false)
        }
    }

    /**
     * Rajoute un écouteur qui écoute le déplacement du carousel
     * @param {*} callback 
     */
    onMove(callback) {
        this.moveCallbacks.push(callback)
    }

    /**
     * Ecouteur de redimensionnement de la fenêtre
     */
    onWindowResize() {
        let mobile = window.innerWidth < 800

        if (mobile !== this.isMobile) {
            this.isMobile = mobile
            this.setStyle()
            this.moveCallbacks.forEach(cb => cb(this.currentItem))
        }
    }

    /**
     * 
     * @param {string} className
     * @returns {HTMLDivElement}
     */
    createDivWithClass(className) {
        const div = document.createElement('div');
        div.className = className
        return div
    }

    /**
     * @returns {number}
     */
    get slidesToScroll() {
        return this.isMobile ? 1 : this.options.slidesToScroll
    }

    /**
     * @returns {number}
     */
    get slidesVisible() {
        return this.isMobile ? 1 : this.options.slidesVisible
    }
}
