// class Carousel {

//     /**
//      * @param {HTMLElement} element 
//      * @param {Object} options
//      * @param {number} [options.slidesToScroll=1] Nombre d'élément à faire défiler
//      * @param {number} [options.slidesVisible=1] Nombre d'élément visible dans un slide
//      * @param {boolean} [options.loop=false] Le carousel doit il boucler
//      */
//     constructor(element, options = {}) {
//         this.element = element;
//         this.options = Object.assign({}, {
//             slidesToScroll: 1,
//             slidesVisible: 1,
//             loop: false
//         }, options)
        
//         const children = [].slice.call(element.children)
//         this.isMobile = false
//         this.currentItem = 0
//         this.moveCallbacks = []

//         // DOM Modification
//         this.root = this.createDivWithClass('carousel')
//         this.container = this.createDivWithClass('carousel-container')
//         this.root.setAttribute("tabindex", '0')
        
//         this.root.appendChild(this.container)
//         this.element.appendChild(this.root)
//         this.items = children.map((child) => {
//             const item = this.createDivWithClass('carousel-item')
//             item.appendChild(child)
//             this.container.appendChild(item);
//             return item
//         })

//         this.setStyle()
//         this.createNavigation()

//         // Events
//         this.moveCallbacks.forEach(cb => cb(0))
//         this.onWindowResize()
//         window.addEventListener('resize', this.onWindowResize.bind(this))
//         this.root.addEventListener("keyup", e => {
//             if (e.key === "ArrowRight") this.nextActionButton()
//             else if (e.key === "ArrowLeft") this.prevActionButton()
//         })
//     }

//     setStyle() {
//         const ratio = this.items.length / this.slidesVisible
//         this.container.style.width = (ratio * 100) + "%"
//         this.items.forEach(item => item.style.width = ((100 / this.slidesVisible) / ratio) + "%");
//     }

//     createNavigation() {
//         const nextButton = this.createDivWithClass('carousel-next')
//         const prevButton = this.createDivWithClass('carousel-prev')
//         this.root.appendChild(nextButton)
//         this.root.appendChild(prevButton)

//         nextButton.addEventListener('click', this.nextActionButton.bind(this))
//         prevButton.addEventListener('click', this.prevActionButton.bind(this))

//         if (!this.options.loop) {
//             this.onMove(index => {
//                 prevButton.classList.toggle("carousel-prev--hidden", index === 0)
//                 nextButton.classList.toggle("carousel-next--hidden", this.items[this.currentItem + this.slidesVisible] === undefined)
//             })
//         }
//     }

//     nextActionButton() {
//         this.gotoItem(this.currentItem + this.slidesToScroll)
//     }

//     prevActionButton() {
//         this.gotoItem(this.currentItem - this.slidesToScroll)
//     }

//     /**
//      * Déplace le carousel vers l'élément ciblé
//      * @param {number} index 
//      */
//     gotoItem(index) {
//         if (index < 0) {
//             if (this.options.loop) index = this.items.length - this.slidesVisible
//             else return
//         } else if (index >= this.items.length || (this.items[this.currentItem + this.slidesVisible] === undefined && index > this.currentItem)) {
//             if (this.options.loop) index = 0
//             else return
//         }
//         const translateX = index * -100 / this.items.length
//         this.container.style.transform = 'translate3d('+translateX+'%, 0, 0)'
//         this.currentItem = index
//         this.moveCallbacks.forEach(cb => cb(index))
//     }

//     onMove(callback) {
//         this.moveCallbacks.push(callback)
//     }

//     onWindowResize() {
//         let mobile = window.innerWidth < 800

//         if (mobile !== this.isMobile) {
//             this.isMobile = mobile
//             this.setStyle()
//             this.moveCallbacks.forEach(cb => cb(this.currentItem))
//         }
//     }

//     /**
//      * 
//      * @param {string} className
//      * @returns {HTMLDivElement}
//      */
//     createDivWithClass(className) {
//         const div = document.createElement('div');
//         div.className = className
//         return div
//     }

//     /**
//      * @returns {number}
//      */
//     get slidesToScroll() {
//         return this.isMobile ? 1 : this.options.slidesToScroll
//     }

//     /**
//      * @returns {number}
//      */
//     get slidesVisible() {
//         return this.isMobile ? 1 : this.options.slidesVisible
//     }
// }


(function () {
    document.addEventListener("DOMContentLoaded", function () {
        window.addEventListener("scroll", function () {
            const navbar = document.querySelector(".nav");
            const stickyScroll = 200;
            navbar.classList.toggle("sticky", this.scrollY > stickyScroll);
            navbar.classList.toggle("shadow-1", this.scrollY > stickyScroll);

            document.querySelector(".scroll-up-btn").classList.toggle("show", this.scrollY > 500);
        });
    });

    createBarLine();
    new Carousel(document.querySelector(".teams-content"), {
        slidesToScroll: 2,
        slidesVisible: 2,
        loop: true,
        pagination: true,
        // infinite: true
    });
})();
